#ifndef PATCH_H
#define PATCH_H

#include "rand.h"
#include <iostream>

using namespace std;

/* SIR individual
 * status: -1 R
 *          0 S
 *          1 I
 *          */
class SIRBase
{
    public:
        int status;
    SIRBase(): status(0)
    {
    }
};


#endif
