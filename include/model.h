#ifndef MODEL_H
#define MODEL_H

#include "snaphelper.h"
#include "patch.h"
#include <cmath>

class BasicSIR
{
    public:
        typedef Network<SIRBase> NetType;

        double r;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::RandDouble randd;

        BasicSIR(double con_num, PUNGraph graph):
            r(con_num),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            randd()
        {
        }

        void seed(double seed_ratio)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(randd.gen() < seed_ratio)
                {
                    node_iter.getNode().status = 1;
                }
            }
        }

        void seed(int seeds)
        {
            for(int i=0; i<seeds; ++i)
            {
                getNodeIter().getNode().status = 1;
            }
        }

        int sim()
        {
            while(true)
            {
                oneMCStep();
                if(getInfectedNodes() < 1)
                    break;
            }
            return getRecoveryNodes();
        }

        void oneMCStep()
        {
            for(int i=0; i<net_size; ++i)
                oneTimeStep();
        }

        void oneTimeStep()
        {
            NetType::NodeIterator the_node_iter = getNodeIter();
            SIRBase & the_node = the_node_iter.getNode();
            if(the_node.status == 1)
            {
                for(int i=0; i<the_node_iter->GetDeg(); ++i)
                {
                    SIRBase & neighbor = getNodeIter(the_node_iter->GetNbrNId(i)).getNode();
                    if(neighbor.status == 0)
                    {
                        if(randd.gen() < r)
                            neighbor.status = 1;
                    }
                }

                the_node.status = -1;
            }
        }

        void oldoneTimeStep()
        {
            NetType::NodeIterator the_node_iter = getNodeIter();
            SIRBase & the_node = the_node_iter.getNode();
            if(the_node.status == 1)
            {
                the_node.status = -1;
            }
            else if(the_node.status == 0)
            {
                int infected_neighbors = getInfectedNeighbors(the_node_iter);
                if(infected_neighbors>0)
                {
                    if(randd.gen() < (1-std::pow(1-r, infected_neighbors)))
                        the_node.status = 1;
                }
            }
        }

        int getInfectedNeighbors(NetType::NodeIterator & node_iter)
        {
            int base(0);
            for(int i=0; i<node_iter->GetDeg(); ++i)
            {
                if(getNodeIter(node_iter->GetNbrNId(i)).getNode().status == 1)
                {
                    base += 1;
                }
            }
            return base;
        }

        NetType::NodeIterator getNodeIter(int id)
        {
            return net.getNodeFromId(id);
        }

        NetType::NodeIterator getNodeIter()
        {
            return getNodeIter(rand_patch.gen());
        }

        int getInfectedNodes()
        {
            int base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().status == 1)
                    base += 1;
            }
            return base;
        }

        int getRecoveryNodes()
        {
            int base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().status == -1)
                    base += 1;
            }
            return base;
        }
        
        std::pair<int, int> getSysStatus()
        {
            int is(0), rs(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                int node_status = node_iter.getNode().status;
                if(node_status==1)
                    is += 1;
                if(node_status==-1)
                    rs += 1;
            }
            return std::make_pair(is, rs);
        }
};

#endif
