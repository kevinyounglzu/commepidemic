# -*- coding: utf8 -*-
import os.path
import sys
import numpy as np


def testFileExist(file_url):
    if not os.path.isfile(file_url):
        print "%s dosen't exsit, abort..." % file_url
        sys.exit()


def resolveMeta(file_url):
    testFileExist(file_url)
    with open(file_url) as meta:
        return sorted(map(int, meta.readlines()))


def resloveLog(file_url):
    testFileExist(file_url)
    with open(file_url) as log_files:
        logs = log_files.readlines()[1:]
    times = []
    jobs = []
    for log in logs:
        log = log.split()
        times.append(float(log[3]))
        jobs.append(int(log[-1]))
    return np.asarray(times), set(jobs)


def resloveMakefile(file_url):
    testFileExist(file_url)
    with open(file_url) as make:
        for line in make.readlines():
            if line.startswith("JOBS"):
                cores = int(line[7:-1])
            if line.startswith("LOGFILE"):
                log_file = line[8:-1]
            if line.startswith("CONFIGFILE"):
                config_meta = line[11:-1]

    return cores, log_file, config_meta

if __name__ == "__main__":
    makefile_url = "./Makefile"
    cores, log_file_url, config_meta = resloveMakefile(makefile_url)

    jobs_queue = resolveMeta(config_meta)
    jobs = len(jobs_queue)
    times, finished_jobs_set = resloveLog(log_file_url)
    unfinished_jobs = sorted(list(set(jobs_queue) - finished_jobs_set))

    # profile
    if len(sys.argv) < 2:
        jobs_finished = len(finished_jobs_set) / float(jobs) * 100
        if len(times) > 0:
            mean_time = np.mean(times)
            time_sd = np.sqrt(np.var(times))
        else:
            mean_time = 0.0
            time_sd = 0.0
        whole_time = mean_time * jobs / 3600. / float(cores)
        time_to_go = whole_time * (1-jobs_finished/100.)

        print "Cores        : %d" % cores
        print "Jobs finished: %.2f %%" % jobs_finished
        print "Mean time    : %.0f s" % mean_time
        print "Time sd      : %.0f s" % time_sd
        if time_to_go < 1:
            print "Time est     : %.2f min / %.2f hr" % (time_to_go * 60, whole_time)
        else:
            print "Time est     : %.2f hr / %.2f hr" % (time_to_go, whole_time)
        print "Unfinished jobs %d: " % len(unfinished_jobs)
        print unfinished_jobs[:cores]

    # backup
    elif len(sys.argv) == 2:
        with open("./config/metabackup.cfg", "w") as f:
            for i in range(jobs):
                f.write("%d\n" % i)

        with open("./config/meta.cfg", "w") as f:
            for i in unfinished_jobs:
                f.write("%d\n" % i)
