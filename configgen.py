# -*- coding: utf8 -*-

import numpy as np


def genSIRRegularOneSeed():
    size = 1000
    degree = 4

    repeat = 10000

    rs = np.linspace(0., 1, 201)

    counter = 0
    for r in rs:
        filename = "./config/%d.cfg" % counter
        with open(filename, "w") as f:
            f.write("int size=%d\n" % size)
            f.write("int degree=%d\n" % degree)

            f.write("double r=%f\n" % r)

            f.write("int repeat=%d\n" % repeat)
            counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSIRRegularOneSeed():
    size = 1000
    degree = 4

    repeat = 5000

    qs = [0.01, 0.1]
    rs = np.linspace(0., 1, 201)

    counter = 0
    for q in qs:
        for r in rs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double r=%f\n" % r)
                f.write("double q=%f\n" % q)

                f.write("int repeat=%d\n" % repeat)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)

if __name__ == "__main__":
    genSIRRegularOneSeed()
