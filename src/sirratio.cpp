#include "rand.h"
#include "parser.h"

#include "Snap.h"
#include "snaphelper.h"
#include "patch.h"
#include "model.h"

using namespace std;

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    int repeat = parser.getInt("repeat");
    double infectious_rate = parser.getDouble("r");
    double q = parser.getDouble("q");


    ofstream out;
    out.open(mylib::tapeFileName("./data/sirratio/", ".txt", argv[1]));

    out << "# r=" << infectious_rate << endl;
    out << "# q=" << q << endl;
    out << "# d=" << degree << endl;

    PUNGraph graph = TSnap::GenRndDegK(size, degree);
    for(int i=0; i<repeat; ++i)
    {
        BasicSIR bsir(infectious_rate, graph);
        bsir.seed(q);
        out << bsir.sim() << endl;
    }
//    for(int i=0; i<steps; ++i)
//    {
//        bsir.oneMCStep();
//        std::pair<int, int> result = bsir.getSysStatus();
//        cout << result.first << " " << result.second << endl;
//    }
}
